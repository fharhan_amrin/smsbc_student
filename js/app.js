//var full_base_name = "http://localhost:8080/smsbc/",historyurl = [];


// const link_local= 'http://localhost';
// const link_local='http://150.242.111.235';


var link_local;
if (window.location.hostname == "localhost") {
  link_local = "http://localhost:8080";
  // link_local = "http://localhost";
} else {
  link_local = "http://172.20.212.17";
}

console.log('IP', link_local);

var full_base_name = link_local+"/smsbc/",historyurl = [];
$.pages = "pages/";

$(document).ready(function() {
  // $(".breadcrumb").hide();
  $("#titlePage").html("Dashboard");
  $("#openedPage").html("Dashboard");
  $("#modulePage").attr("style", "display:none");
  var fl = localStorage["firstLog"];

  if (fl == "true") {
    $(".page-title").attr("style", "display:none");
    $(".breadcrumb").attr("style", "display:none");
    $(".side-menu").attr("style", "display:none");
    $(".content-page").attr("style", "margin-left:0");
    $(".konten").load("dashboard.html");
  } else {
    
  }
  // dd = getDataLogin();
  $("#loader").fadeOut("fast");
});

function setInputDate(_id){
  var _dat = document.querySelector(_id);
  var hoy = new Date(),
      d = hoy.getDate(),
      m = hoy.getMonth()+1, 
      y = hoy.getFullYear(),
      data;

  if(d < 10){
      d = "0"+d;
  };
  if(m < 10){
      m = "0"+m;
  };

  data = y+"-"+m+"-"+d;
  console.log(data);
  _dat.value = data;
};


function cancelTransaction(params,judul) {
  var url,
      title;
  if (params==undefined) {
    url = 'po/purchase_order.html';
    title = "Purchase Order";
  }else{
    url = params;
    title = judul;
  }
  var modul = 'Purchase Order';

    moveUrl(url, modul, title, '', '');
}

function resetData() {
  swal({
    title: 'Are you sure want to delete all data ?',
    showCancelButton: true,
    confirmButtonText: 'Submit',
    showLoaderOnConfirm: true,
    confirmButtonClass: 'btn btn-success',
    cancelButtonClass: 'btn btn-danger m-l-10',
    preConfirm: function () {
      return new Promise(function (resolve, reject) {
        setTimeout(function () {
          $.ajax({
            url: api_url + 'Data/resetAllData',
            dataType: 'json',
            method: 'post',
            success: function (d) {
              if (d.success) {
                resolve();

              } else {
                returnErrorMessage(d.msg);
                reject();
              }
            },
            error: function (xhr) {
              returnErrorMessage("Oops! " + xhr.status + " " + xhr.statusText);
              reject();
            }
          })
        }, 2000)
      })
    },
    allowOutsideClick: false
  }).then(function () {
    swal({
      type: 'success',
      title: 'All data Payment & PO has been reset!',
      showCancelButton: false,
      confirmButtonColor: '#4fa7f3'
    }).then(function () {
      var url = 'dashboard.html',
        modul = 'Dashboard',
        title = 'Dashboard';
      moveUrl(url, modul, title);
    })
  })
}

function rejectTrx(id) {
    console.log(id);
    var btn = $("#btnReject");
    var status = 2;
    if (btn.data("by") == "ps") {
        status = 3;
    }
    if(id==undefined){
      id = btn.data('id');
    }
    var ids = localStorage['position_id'];
    if (ids != 8 || ids != 1) {
        swal({
            title: 'Are you sure want to reject purchase order ?',
            showCancelButton: true,
            input: 'text',
            text: 'Reason : ',
            confirmButtonText: 'Submit',
            showLoaderOnConfirm: true,
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger m-l-10',
            preConfirm: function (reason) {
                return new Promise(function (resolve, reject) {
                    setTimeout(function () {
                        $.ajax({
                            url: api_url + 'purchase/rejectPOArco',
                            data: { id: id, status: status, reason: reason, idu: localStorage['id'] },
                            dataType: 'json',
                            method: 'post',
                            success: function (d) {
                                if (d.success) {
                                    resolve();

                                } else {
                                    returnErrorMessage(d.msg);
                                    reject();
                                }
                            },
                            error: function (xhr) {
                                returnErrorMessage("Oops! " + xhr.status + " " + xhr.statusText);
                                reject();
                            }
                        })
                    }, 2000)
                })
            },
            allowOutsideClick: false
        }).then(function () {
            swal({
                type: 'success',
                title: 'Purchase Order has been rejected!',
                showCancelButton: false,
                confirmButtonColor: '#4fa7f3'
            }).then(function () {
                var url = 'po/purchase_order.html',
                    modul = 'Purchase Order',
                    title = 'Purchase Order';
                moveUrl(url, modul, title);
            })
        })
    } else {
        toastr.warning("You don't have privilege to access this module", 'Sorry', { 'positionClass': 'toast-top-right' });
    }
}

$(document).ready(function () {
    // checkRules();
    $("#sidebar ul li a.primary-menu").click(function (event) {
        event.preventDefault();
        event.stopPropagation();
        var url = $(this).attr('href'),
                modul = $(this).data('modul'),
                title = $(this).data('title'),
                liparent = $(this).parent();
        
        $.ajax({
            type: 'GET',
            url: url + '?u=' + Math.random(),
            beforeSend: function () {
                cekSession();
                $(".breadcrumb").fadeIn();
                $("#sidebar ul li").removeClass('active');

                $(".konten").html('<center><div style="margin-top:25vh;" class="loader"></div></center>');
            },
            error: function () {
                $("#titlePage").html(title);
                $("#openedPage").html(title);
                $("#modulePage").html(modul);
                $(".konten").html('<div class="content-wrapper d-flex align-items-center text-center error-page bg-primary"> <div class="row flex-grow"> <div class="col-lg-7 mx-auto text-white"> <div class="row align-items-center d-flex flex-row"> <div class="col-lg-6 text-lg-right pr-lg-4"> <h1 class="display-1 mb-0">404</h1> </div> <div class="col-lg-6 error-page-divider text-lg-left pl-lg-4"> <h2>SORRY!</h2> <h3 class="font-weight-light">The page you’re looking for was not found.</h3> </div> </div> <div class="row mt-5"> <div class="col-12 text-center mt-xl-2"> <a class="text-white font-weight-medium" href="'+full_base_name+'">Back to home</a> </div> </div>'); },
            success: function (data) {
                liparent.addClass('active');
                runPages();
                $("#titlePage").html(title);
                $("#openedPage").html(title);
                $(this).parent().addClass('active');
                if (modul == '') {
                    $("#modulePage").attr('style', 'display:none');
                } else {
                    $("#modulePage").attr('style', 'display:block').html(modul);
                }
                document.title = "SMS Broadcast - " + title;
                $(".konten").fadeOut(300, function () {
                    $(".konten").html(data).delay(100).fadeIn(300);
                })
            }
        });
      })
  });

// ini
function checkRules() {
  if (localStorage["status"] == "login") {
    var allowUrl = localStorage["allowUrl"].toString().split("[|]");

    // var listMenu = $('#sidebar li:not(:first) a');
    var listMenu = $("#sidebar li a.menuhidden");
    for (var ls in listMenu) {
      var mn = listMenu.eq(ls);

      if (mn.attr("href") != "javascript:void(0);") {
        mn.addClass("hideForever");
      }
    }

    for (var au in allowUrl) {
      var al = allowUrl[au];

      //        console.log('#sidebar a[href="' + al + '"]');

      $('#sidebar a[href="' + al + '"]').removeClass("hideForever");
    }

    $(".hideForever").remove();

    var listParent = $("#sidebar ul");

    for (var lp in listParent) {
      var ml = listParent.eq(lp);

      if (ml.find("li a").length == 0) {
        ml.parent().remove();
      }
    }

    listParent = $("#sidebar ul");
    for (var lp in listParent) {
      var ml = listParent.eq(lp);

      if (ml.find("li a").length == 0) {
        ml.parent().remove();
      }
    }
  }
}

function setSessionLogout() {
  localStorage.clear();
  localStorage.removeItem("username");
  localStorage.removeItem("password");
  localStorage.removeItem("name");
  localStorage.removeItem("first_login");
}
function cekSession() {
  var bl = localStorage["first_login"];
    // id = localStorage["id"];
  if (
    bl == "" ||
    bl == "false" ||
    bl == undefined
  ) {
    setSessionLogout();
    localStorage.setItem("sessionTimedOut", "true");

    window.location.href = "../";
  } else {
  }
}

function moveUrl(url, modul, title, id, callback) {
  $.ajaxSetup({
    global: true,
    beforeSend: function(jqXHR, settings) {
      $(".content-page").append(
        '<div id="loaderNew" class="loading style-2"><div class="loading-wheel"></div></div>'
      );
      $("#loaderNew").fadeIn("slow");
    },
    complete: function() {
      $("#loaderNew").remove();
    },
    error: function() {
      $("#loaderNew").remove();
    }
  });
  $.ajax({
    type: "GET",
    url: url + "?u=" + Math.random(),
    beforeSend: function() {
      $(".konten div").remove();
      $(".breadcrumb").fadeIn();
      $(".konten").html(
        '<center><div style="margin-top:25vh;" class="loader"></div></center>'
      );
    },
    error: function() {
      $(".konten").html(
        '<div class="wrapper-page"> <div class="ex-page-content text-center"> <div class="text-error"><span class="text-primary">4</span><i class="ti-face-sad text-pink"></i><span class="text-info">4</span></div> <h2>Who0ps! Page not found</h2><br> <p class="text-muted">This page cannot found or is missing.</p> <p class="text-muted">Use the navigation above or the button below to get back and track.</p> <br> <a class="btn btn-default waves-effect waves-light" href="index.html"> Return Home</a> </div> </div> '
      );
    },
    success: function(data) {
      $("#titlePage").html(title);
      $(".dta").html("<input type='hidden' id='m_id' value='" + id + "' >");
      $("#openedPage").html(title);
      $("#modulePage").html(modul);
      $(".konten").fadeOut(300, function() {
        $(".konten")
          .html(data)
          .delay(100)
          .fadeIn(300);
        try {
          if (typeof id == "function") {
            id();
          } else if (typeof callback == "function") {
            callback();
          }
        } catch (e) {
          console.log(e);
        }
      });
    }
  });
}

function runPages() {
  $.ajaxSetup({
    global: true,
    beforeSend: function(jqXHR, settings) {
      $(".content-page").append(
        '<div id="loaderNew" class="loading style-2"><div class="loading-wheel"></div></div>'
      );
      $("#loaderNew").fadeIn("slow");
    },
    complete: function() {
      $("#loaderNew").remove();
    },
    error: function() {
      $("#loaderNew").remove();
    }
  });

  // $('.datepicker-autoclose').daterangepicker({
  //     buttonClasses: ['btn', 'btn-sm'],
  //     applyClass: 'btn-default',
  //     cancelClass: 'btn-white'
  // });
  var start = moment().subtract(29, 'days');
  var end = moment();
  $("input[type='datetimepicker']").daterangepicker({
    startDate: moment().startOf('month'),
    endDate: end
  });
  $(".select2").select2();
  // $('.tagsinput').tagsinput();

  $(".select2-limiting").select2({
    maximumSelectionLength: 2
  });

  // $('.selectpicker').selectpicker();

  // $('form').parsley();

  $(document)
    .off("click", '[data-plugin="custommodal"]')
    .on("click", '[data-plugin="custommodal"]', function(e) {
      Custombox.open({
        target: $(this).attr("href"),
        effect: $(this).attr("data-animation"),
        overlaySpeed: $(this).attr("data-overlaySpeed"),
        overlayColor: $(this).attr("data-overlayColor")
      });
      e.preventDefault();
    });
  $("a.action-menu")
    .unbind("click")
    .click(function(e) {
      cekSession();
      e.preventDefault();
      var url = $(this).attr("href");
      (modul = $(this).data("modul")), (title = $(this).data("title"));
      document.title = "Bonita - " + title;

      moveUrl(url, modul, title);
    });
  $("[data-toggle='tooltip']").tooltip();
}

// UTILITIES

function replaceDuitTigaDigit(str) {
  if (str != null) {

    var depan = str.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return depan;
  } else {
    return "";
  }
}
function runDataTable() {
  if ($.fn.dataTable.isDataTable(".dataTable")) {
    $(".dataTable").DataTable({
      info: false
    });
  } else {
    $(".dataTable").DataTable({
      info: false
    });
  }
}
function runDataTableCustom(params) {
  if (params==undefined) {
    params = 1;
  }
  if ($.fn.dataTable.isDataTable(".dataTable")) {
    $(".dataTable").DataTable({
      info: false,
      order: [[params, "desc"]]
    });
  } else {
    $(".dataTable").DataTable({
      info: false,
      order: [[params, "desc"]]
    });
  }
}

function runDataTableCustomAllocation() {
  if ($.fn.dataTable.isDataTable(".dataTable")) {
    $(".dataTable").DataTable({
      info: false,
      order: [[3, "desc"]]
    });
  } else {
    $(".dataTable").DataTable({
      info: false,
      order: [[3, "desc"]]
    });
  }
}

function editPO(idpo) {
  if (idpo==undefined) {
    var btn = $("#btnEdit");
    idpo = btn.data('id');
  }
  var url = "po/addPurchase.html",
    modul = "Purchase Order",
    title = "Edit Purchase Order";
  moveUrl(
    url,modul,title,function() {
      hasEdit(idpo);
    }
  )
}

function runDataTableCheck() {
  $(".dataTableCheck").DataTable({
    columnDefs: [
      {
        orderable: false,
        className: "select-checkbox",
        targets: 0
      }
    ],
    select: {
      style: "os",
      selector: "td:first-child"
    },
    rowCallback: function(row, data) {
      if ($.inArray(data.DT_RowId, selected) !== -1) {
        $(row).addClass("selected");
      }
    },
    order: [[1, "asc"]]
  });
}

function hasDecimal(num) {
  return !!(num % 1);
}


function datePHPJS(format, timestamp) {
  var that = this;
  var jsdate, f;
  // Keep this here (works, but for code commented-out below for file size reasons)
  // var tal= [];
  var txt_words = [
    "Minggu",
    "Senin",
    "Selasa",
    "Rabu",
    "Kamis",
    "Jumat",
    "Sabtu",
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember"
  ];
  // trailing backslash -> (dropped)
  // a backslash followed by any character (including backslash) -> the character
  // empty string -> empty string
  var formatChr = /\\?(.?)/gi;
  var formatChrCb = function(t, s) {
    return f[t] ? f[t]() : s;
  };
  var _pad = function(n, c) {
    n = String(n);
    while (n.length < c) {
      n = "0" + n;
    }
    return n;
  };
  f = {
    // Day
    d: function() {
      // Day of month w/leading 0; 01..31
      return _pad(f.j(), 2);
    },
    D: function() {
      // Shorthand day name; Mon...Sun
      return f.l().slice(0, 3);
    },
    j: function() {
      // Day of month; 1..31
      return jsdate.getDate();
    },
    l: function() {
      // Full day name; Monday...Sunday
      return txt_words[f.w()];
    },
    N: function() {
      // ISO-8601 day of week; 1[Mon]..7[Sun]
      return f.w() || 7;
    },
    S: function() {
      // Ordinal suffix for day of month; st, nd, rd, th
      var j = f.j();
      var i = j % 10;
      if (i <= 3 && parseInt((j % 100) / 10, 10) == 1) {
        i = 0;
      }
      return ["st", "nd", "rd"][i - 1] || "th";
    },
    w: function() {
      // Day of week; 0[Sun]..6[Sat]
      return jsdate.getDay();
    },
    z: function() {
      // Day of year; 0..365
      var a = new Date(f.Y(), f.n() - 1, f.j());
      var b = new Date(f.Y(), 0, 1);
      return Math.round((a - b) / 864e5);
    },
    // Week
    W: function() {
      // ISO-8601 week number
      var a = new Date(f.Y(), f.n() - 1, f.j() - f.N() + 3);
      var b = new Date(a.getFullYear(), 0, 4);
      return _pad(1 + Math.round((a - b) / 864e5 / 7), 2);
    },
    // Month
    F: function() {
      // Full month name; January...December
      return txt_words[6 + f.n()];
    },
    m: function() {
      // Month w/leading 0; 01...12
      return _pad(f.n(), 2);
    },
    M: function() {
      // Shorthand month name; Jan...Dec
      return f.F().slice(0, 3);
    },
    n: function() {
      // Month; 1...12
      return jsdate.getMonth() + 1;
    },
    t: function() {
      // Days in month; 28...31
      return new Date(f.Y(), f.n(), 0).getDate();
    },
    // Year
    L: function() {
      // Is leap year?; 0 or 1
      var j = f.Y();
      return ((j % 4 === 0) & (j % 100 !== 0)) | (j % 400 === 0);
    },
    o: function() {
      // ISO-8601 year
      var n = f.n();
      var W = f.W();
      var Y = f.Y();
      return Y + (n === 12 && W < 9 ? 1 : n === 1 && W > 9 ? -1 : 0);
    },
    Y: function() {
      // Full year; e.g. 1980...2010
      return jsdate.getFullYear();
    },
    y: function() {
      // Last two digits of year; 00...99
      return f
        .Y()
        .toString()
        .slice(-2);
    },
    // Time
    a: function() {
      // am or pm
      return jsdate.getHours() > 11 ? "pm" : "am";
    },
    A: function() {
      // AM or PM
      return f.a().toUpperCase();
    },
    B: function() {
      // Swatch Internet time; 000..999
      var H = jsdate.getUTCHours() * 36e2;
      // Hours
      var i = jsdate.getUTCMinutes() * 60;
      // Minutes
      var s = jsdate.getUTCSeconds(); // Seconds
      return _pad(Math.floor((H + i + s + 36e2) / 86.4) % 1e3, 3);
    },
    g: function() {
      // 12-Hours; 1..12
      return f.G() % 12 || 12;
    },
    G: function() {
      // 24-Hours; 0..23
      return jsdate.getHours();
    },
    h: function() {
      // 12-Hours w/leading 0; 01..12
      return _pad(f.g(), 2);
    },
    H: function() {
      // 24-Hours w/leading 0; 00..23
      return _pad(f.G(), 2);
    },
    i: function() {
      // Minutes w/leading 0; 00..59
      return _pad(jsdate.getMinutes(), 2);
    },
    s: function() {
      // Seconds w/leading 0; 00..59
      return _pad(jsdate.getSeconds(), 2);
    },
    u: function() {
      // Microseconds; 000000-999000
      return _pad(jsdate.getMilliseconds() * 1000, 6);
    },
    // Timezone
    e: function() {
      // Timezone identifier; e.g. Atlantic/Azores, ...
      // The following works, but requires inclusion of the very large
      // timezone_abbreviations_list() function.
      /*              return that.date_default_timezone_get();
       */
      throw "Not supported (see source code of date() for timezone on how to add support)";
    },
    I: function() {
      // DST observed?; 0 or 1
      // Compares Jan 1 minus Jan 1 UTC to Jul 1 minus Jul 1 UTC.
      // If they are not equal, then DST is observed.
      var a = new Date(f.Y(), 0);
      // Jan 1
      var c = Date.UTC(f.Y(), 0);
      // Jan 1 UTC
      var b = new Date(f.Y(), 6);
      // Jul 1
      var d = Date.UTC(f.Y(), 6); // Jul 1 UTC
      return a - c !== b - d ? 1 : 0;
    },
    O: function() {
      // Difference to GMT in hour format; e.g. +0200
      var tzo = jsdate.getTimezoneOffset();
      var a = Math.abs(tzo);
      return (
        (tzo > 0 ? "-" : "+") + _pad(Math.floor(a / 60) * 100 + (a % 60), 4)
      );
    },
    P: function() {
      // Difference to GMT w/colon; e.g. +02:00
      var O = f.O();
      return O.substr(0, 3) + ":" + O.substr(3, 2);
    },
    T: function() {
      // Timezone abbreviation; e.g. EST, MDT, ...
      return "UTC";
    },
    Z: function() {
      // Timezone offset in seconds (-43200...50400)
      return -jsdate.getTimezoneOffset() * 60;
    },
    // Full Date/Time
    c: function() {
      // ISO-8601 date.
      return "Y-m-d\\TH:i:sP".replace(formatChr, formatChrCb);
    },
    r: function() {
      // RFC 2822
      return "D, d M Y H:i:s O".replace(formatChr, formatChrCb);
    },
    U: function() {
      // Seconds since UNIX epoch
      return (jsdate / 1000) | 0;
    }
  };
  this.date = function(format, timestamp) {
    that = this;
    jsdate =
      timestamp === undefined
        ? new Date() // Not provided
        : timestamp instanceof Date
        ? new Date(timestamp) // JS Date()
        : new Date(timestamp * 1000); // UNIX timestamp (auto-convert to int)
    return format.replace(formatChr, formatChrCb);
  };
  return this.date(format, timestamp);
}

function number_format(number, decimals, decPoint, thousandsSep) {
  number = (number + "").replace(/[^0-9+\-Ee.]/g, "");
  var n = !isFinite(+number) ? 0 : +number;
  var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals);
  var sep = typeof thousandsSep === "undefined" ? "," : thousandsSep;
  var dec = typeof decPoint === "undefined" ? "." : decPoint;
  var s = "";

  var toFixedFix = function(n, prec) {
    var k = Math.pow(10, prec);
    return "" + (Math.round(n * k) / k).toFixed(prec);
  };

  // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
  s = (prec ? toFixedFix(n, prec) : "" + Math.round(n)).split(".");
  if (s[0].length > 3) {
    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
  }
  if ((s[1] || "").length < prec) {
    s[1] = s[1] || "";
    s[1] += new Array(prec - s[1].length + 1).join("0");
  }

  return s.join(dec);
}

function returnErrorMessage(msg) {
  toastr.warning(msg, "Sorry", {
    positionClass: "toast-top-right",
    preventDuplicates: true,
    maxOpened: 1,
    preventOpenDuplicates: true
  });
}
function returnSuccessMessage(msg) {
  toastr.success(msg, "Success", {
    positionClass: "toast-top-right",
    preventDuplicates: true,
    maxOpened: 1,
    preventOpenDuplicates: true
  });
}
function returnErrorExist() {
  toastr.warning("This data has been followed up", "Sorry", {
    positionClass: "toast-top-right"
  });
}
function returnErrorPrivileges() {
  toastr.warning("You don't have privilege to access this module", "Sorry", {
    positionClass: "toast-top-right"
  });
}

function showErrorWithNoAction(m) {
  swal({
    title: "Error",
    text: m,
    type: "error",

    showCancelButton: false,
    showConfirmButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Ok",
    closeOnConfirm: false
  });
  return false;
}

function financial(x) {
  return Number.parseFloat(x).toFixed(2);
}
function replaceAll(str, find, replace) {
  return str.split(find).join(replace);
}

function nulldefault(value) {
    return (value == null) ? "" : value
}

function replaceDuitTigaDigit(str) {
  if (str != null) {
    var depan = str.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return depan;
  } else {
    return "";
  }
}

function replaceDuit(duit) {
  // console.log(duit,'satu');
  duit = duit.toFixed(3).replace(/./g, function(c, i, a) {
    return i && c !== "." && (a.length - i) % 4 === 0 ? "," + c : c;
  });
  // console.log(duit,'dua');
  var duitbaru = duit.toString();
  var split = duitbaru.split(".");
  var depan = split[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  duit = depan + "," + Math.ceil(parseFloat(split[1]) / 10);
  // duit = Math.ceil(duit);
  // console.log(duit,'tiga');
  return duit;
  // return duit.toFixed(2).replace(/./g, function(c, i, a) {
  //     return i && c !== "." && ((a.length - i) % 3 === 0) ? ',' + c : c;
  // });
  // return duit;
}

function commarize(val) {
  // 1e6 = 1 Million, begin with number to word after 1e6.
  var uang = val / 1000;
  var nf = number_format(uang, "2", ".", ",");
  return nf + " K";
}

function strtotime(text, now) {
  var parsed,
    match,
    today,
    year,
    date,
    days,
    ranges,
    len,
    times,
    regex,
    i,
    fail = false;

  if (!text) {
    return fail;
  }

  // Unecessary spaces
  text = text
    .replace(/^\s+|\s+$/g, "")
    .replace(/\s{2,}/g, " ")
    .replace(/[\t\r\n]/g, "")
    .toLowerCase();

  match = text.match(
    /^(\d{1,4})([\-\.\/\:])(\d{1,2})([\-\.\/\:])(\d{1,4})(?:\s(\d{1,2}):(\d{2})?:?(\d{2})?)?(?:\s([A-Z]+)?)?$/
  );

  if (match && match[2] === match[4]) {
    if (match[1] > 1901) {
      switch (match[2]) {
        case "-": {
          if (match[3] > 12 || match[5] > 31) {
            return fail;
          }

          return (
            new Date(
              match[1],
              parseInt(match[3], 10) - 1,
              match[5],
              match[6] || 0,
              match[7] || 0,
              match[8] || 0,
              match[9] || 0
            ) / 1000
          );
        }
        case ".": {
          return fail;
        }
        case "/": {
          if (match[3] > 12 || match[5] > 31) {
            return fail;
          }

          return (
            new Date(
              match[1],
              parseInt(match[3], 10) - 1,
              match[5],
              match[6] || 0,
              match[7] || 0,
              match[8] || 0,
              match[9] || 0
            ) / 1000
          );
        }
      }
    } else if (match[5] > 1901) {
      switch (match[2]) {
        case "-": {
          if (match[3] > 12 || match[1] > 31) {
            return fail;
          }

          return (
            new Date(
              match[5],
              parseInt(match[3], 10) - 1,
              match[1],
              match[6] || 0,
              match[7] || 0,
              match[8] || 0,
              match[9] || 0
            ) / 1000
          );
        }
        case ".": {
          if (match[3] > 12 || match[1] > 31) {
            return fail;
          }

          return (
            new Date(
              match[5],
              parseInt(match[3], 10) - 1,
              match[1],
              match[6] || 0,
              match[7] || 0,
              match[8] || 0,
              match[9] || 0
            ) / 1000
          );
        }
        case "/": {
          if (match[1] > 12 || match[3] > 31) {
            return fail;
          }

          return (
            new Date(
              match[5],
              parseInt(match[1], 10) - 1,
              match[3],
              match[6] || 0,
              match[7] || 0,
              match[8] || 0,
              match[9] || 0
            ) / 1000
          );
        }
      }
    } else {
      switch (match[2]) {
        case "-": {
          // YY-M-D
          if (
            match[3] > 12 ||
            match[5] > 31 ||
            (match[1] < 70 && match[1] > 38)
          ) {
            return fail;
          }

          year = match[1] >= 0 && match[1] <= 38 ? +match[1] + 2000 : match[1];
          return (
            new Date(
              year,
              parseInt(match[3], 10) - 1,
              match[5],
              match[6] || 0,
              match[7] || 0,
              match[8] || 0,
              match[9] || 0
            ) / 1000
          );
        }
        case ".": {
          // D.M.YY or H.MM.SS
          if (match[5] >= 70) {
            // D.M.YY
            if (match[3] > 12 || match[1] > 31) {
              return fail;
            }

            return (
              new Date(
                match[5],
                parseInt(match[3], 10) - 1,
                match[1],
                match[6] || 0,
                match[7] || 0,
                match[8] || 0,
                match[9] || 0
              ) / 1000
            );
          }
          if (match[5] < 60 && !match[6]) {
            // H.MM.SS
            if (match[1] > 23 || match[3] > 59) {
              return fail;
            }

            today = new Date();
            return (
              new Date(
                today.getFullYear(),
                today.getMonth(),
                today.getDate(),
                match[1] || 0,
                match[3] || 0,
                match[5] || 0,
                match[9] || 0
              ) / 1000
            );
          }

          return fail;
        }
        case "/": {
          if (
            match[1] > 12 ||
            match[3] > 31 ||
            (match[5] < 70 && match[5] > 38)
          ) {
            return fail;
          }

          year = match[5] >= 0 && match[5] <= 38 ? +match[5] + 2000 : match[5];
          return (
            new Date(
              year,
              parseInt(match[1], 10) - 1,
              match[3],
              match[6] || 0,
              match[7] || 0,
              match[8] || 0,
              match[9] || 0
            ) / 1000
          );
        }
        case ":": {
          if (match[1] > 23 || match[3] > 59 || match[5] > 59) {
            return fail;
          }

          today = new Date();
          return (
            new Date(
              today.getFullYear(),
              today.getMonth(),
              today.getDate(),
              match[1] || 0,
              match[3] || 0,
              match[5] || 0
            ) / 1000
          );
        }
      }
    }
  }

  if (text === "now") {
    return now === null || isNaN(now)
      ? (new Date().getTime() / 1000) | 0
      : now | 0;
  }
  if (!isNaN((parsed = Date.parse(text)))) {
    return (parsed / 1000) | 0;
  }

  date = now ? new Date(now * 1000) : new Date();
  days = {
    sun: 0,
    mon: 1,
    tue: 2,
    wed: 3,
    thu: 4,
    fri: 5,
    sat: 6
  };
  ranges = {
    yea: "FullYear",
    mon: "Month",
    day: "Date",
    hou: "Hours",
    min: "Minutes",
    sec: "Seconds"
  };

  function lastNext(type, range, modifier) {
    var diff,
      day = days[range];

    if (typeof day !== "undefined") {
      diff = day - date.getDay();

      if (diff === 0) {
        diff = 7 * modifier;
      } else if (diff > 0 && type === "last") {
        diff -= 7;
      } else if (diff < 0 && type === "next") {
        diff += 7;
      }

      date.setDate(date.getDate() + diff);
    }
  }

  function process(val) {
    var splt = val.split(" "), // Todo: Reconcile this with regex using \s, taking into account browser issues with split and regexes
      type = splt[0],
      range = splt[1].substring(0, 3),
      typeIsNumber = /\d+/.test(type),
      ago = splt[2] === "ago",
      num = (type === "last" ? -1 : 1) * (ago ? -1 : 1);

    if (typeIsNumber) {
      num *= parseInt(type, 10);
    }

    if (ranges.hasOwnProperty(range) && !splt[1].match(/^mon(day|\.)?$/i)) {
      return date["set" + ranges[range]](date["get" + ranges[range]]() + num);
    }

    if (range === "wee") {
      return date.setDate(date.getDate() + num * 7);
    }

    if (type === "next" || type === "last") {
      lastNext(type, range, num);
    } else if (!typeIsNumber) {
      return false;
    }

    return true;
  }

  times =
    "(years?|months?|weeks?|days?|hours?|minutes?|min|seconds?|sec" +
    "|sunday|sun\\.?|monday|mon\\.?|tuesday|tue\\.?|wednesday|wed\\.?" +
    "|thursday|thu\\.?|friday|fri\\.?|saturday|sat\\.?)";
  regex =
    "([+-]?\\d+\\s" + times + "|" + "(last|next)\\s" + times + ")(\\sago)?";

  match = text.match(new RegExp(regex, "gi"));
  if (!match) {
    return fail;
  }

  for (i = 0, len = match.length; i < len; i++) {
    if (!process(match[i])) {
      return fail;
    }
  }
  return date.getTime() / 1000;
}



