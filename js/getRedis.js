$(document).ready(function() {
  getDataFromRedis();
  getDataBroadcast();
  getDataDU();
  getDataMIQ();
  getDataSubmit();

  chart();
  $("#dateNow").html(datePHPJS("d, M Y", new Date()));
});

function getDataFromRedis() {
  $.ajax({
    type: "GET",

    url: link_local+"/smsbc_student/process/getDashboard.php?f=getDataRedis",

    // url: link_local+"smsbc/process/getDashboard.php?f=getDataRedis",

    dataType: "json",
    success: function(resp) {
      // console.log("resp", resp);
      $("#totalSub").text(resp);
      // $('#todaySub').text(resp.todaySub)
    },
    error: function(resp) {
      console.log("this error", resp);
    }
  });
}

var count = 0


function getDataBroadcast() {
  $.ajax({
    type: "GET",

    url: link_local+"/smsbc_student/process/getDashboard.php?f=getDataBroadcast",

    // url: link_local+"smsbc/process/getDashboard.php?f=getDataBroadcast",

    dataType: "json",
    success: function(resp) {
      var nilai = []

      var tamp = 0;

      resp.data.map((itm, indx) => {
        if (itm.status == "P") {
          $("#pending").text(itm.count);
        } else if (itm.status == "D") {
          $("#done").text(itm.count);
        } else if (itm.status == "R") {
          $("#running").text(itm.count);
        } else if (itm.status == "T") {
          $("#terminate").text(itm.count);
        } 
        tamp = parseInt(tamp) + parseInt(itm.count);
        //window.count + parseInt(itm.count)
        // setInterval(() => {
        //   console.log(itm.count)
            
        //   }, 2000);
      });

      $("#totalbroadcast").text(tamp);

      console.log(tamp);

      // setInterval(() => {
      // console.log(unt)
        
      // }, 2000);
    },
    error: function(resp) {
      console.log("this error", resp);
    }
  });
}

function getDataDU() {
  $.ajax({
    type: "GET",

    url: link_local+"/smsbc_student/process/getDashboard.php?f=getDataDU",

    // url: link_local+"smsbc/process/getDashboard.php?f=getDataDU",

    dataType: "json",
    success: function(resp) {
      var nilai = []

      var tamp = 0;

      // resp.data.map((itm, indx) => {
      //   if (itm.status == "1") {
      if(resp.data[0].dr==null||resp.data[0].dr=='null'){resp.data[0].dr=0;}
      if(resp.data[0].udr==null||resp.data[0].udr=='null'){resp.data[0].udr=0;}

          $("#delivered").text(resp.data[0].dr);
        //} else if (itm.status == "4") {
          $("#undelivered").text(resp.data[0].udr);
      //   }
      // });
    },
    error: function(resp) {
      console.log("this error", resp);
    }
  });
}
function getDataMIQ() {
  $.ajax({
    type: "GET",

    url: link_local+"/smsbc_student/process/getDashboard.php?f=getDataMIQ",

    // url: link_local+"smsbc/process/getDashboard.php?f=getDataMIQ",

    dataType: "json",
    success: function(resp) {
      $("#messque").text(resp);
    },
    error: function(resp) {
      console.log("this error", resp);
    }
  });
}

function getDataSubmit() {
  $.ajax({
    type: "GET",

    url: link_local+"/smsbc_student/process/getDashboard.php?f=getDataSubmit",

    // url: link_local+"process/getDashboard.php?f=getDataSubmit",

    dataType: "json",
    success: function(resp) {
      // console.log("resp", resp);
      if(resp.data[0]==null||resp.data[0]=='null'){resp.data[0]=0;}
      $("#submit").text(resp.data[0]);
      // $('#todaySub').text(resp.todaySub)
    },
    error: function(resp) {
      console.log("this error", resp);
    }
  });
}

function chart(params) {
  Highcharts.chart("container", {
    chart: {
      type: "column"
    },
    title: {
      text: "Stacked column chart"
    },
    xAxis: {
      categories: ["8888", "8989", "8775", "9122", "7495"]
    },
    yAxis: {
      min: 0,
      title: {
        text: "Total Data Succes and Failed"
      },
      stackLabels: {
        enabled: true,
        style: {
          fontWeight: "bold",
          color:
            // theme
            (Highcharts.defaultOptions.title.style &&
              Highcharts.defaultOptions.title.style.color) ||
            "gray"
        }
      }
    },
    legend: {
      align: "right",
      x: -30,
      verticalAlign: "top",
      y: 25,
      floating: true,
      backgroundColor:
        Highcharts.defaultOptions.legend.backgroundColor || "white",
      borderColor: "#CCC",
      borderWidth: 1,
      shadow: false
    },
    tooltip: {
      headerFormat: "<b>{point.x}</b><br/>",
      pointFormat: "{series.name}: {point.y}<br/>Total: {point.stackTotal}"
    },
    plotOptions: {
      column: {
        stacking: "normal",
        dataLabels: {
          enabled: true
        }
      }
    },
    series: [
      {
        name: "Success",
        color: "green",
        data: [5, 3, 4, 7, 2]
      },
      {
        name: "Failed",
        color: "red",
        data: [2, 2, 3, 2, 1]
      }
    ]
  });
}
