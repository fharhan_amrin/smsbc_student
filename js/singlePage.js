$(document).ready(function() {
  callOneTime()
  $("a").on("click", function(e) {
    e.preventDefault();
    var url = $(this).attr("href");
    if (url.charAt(0) != "#") {
      callPage(url);
    }
    console.log(url);
  });
});

var callOneTime = (function() {
  var executed = false;
  return function() {
    if (!executed) {
      executed = true;
      callPage("dashboard.html");

      // do something
    }
  };
})();

function callPage(url) {
  $.ajax({
    url: url,
    type: "GET",
    dataType: "text",
    success: function(response) {
      // console.log("this page was loaded", response);
      $(".content-wrapper").html(response);
    },
    error: function(error) {
      console.log("this page was NOT loaded", error);
    },
    complete: function(xhr, status) {
      console.log("the request complete");
    }
  });
}
