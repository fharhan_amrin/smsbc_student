<?php
include "connection.php";

date_default_timezone_set('asia/jayapura');
header('Access-Control-Allow-Origin:*');
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

$action = @$_GET['action'];
$date = date('Y-m-d H:i:s');


if ($action == 'insert') {
    if($_POST['schedule_type']=='C'){
        $tamp = explode("T", $_POST['schedule']);
        $schedule = $tamp[0] . ' ' . $tamp[1] . ":00";
    }else{
        //$schedule = date('Y-m-d H:i:s');
        $schedule=date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." +20 minutes"));
        
    }

    $fakultas='NULL';
    $jurusan='NULL';
    $nre='NULL';

    if (isset($_POST['fakultas'])){
        if($_POST['fakultas']!=''  || $_POST['fakultas']!=null) {
            $fakultas=$_POST['fakultas'];
        }
    }
    if (isset($_POST['jurusan'])){
        if($_POST['jurusan']!=''  || $_POST['jurusan']!=null) {
            $jurusan=$_POST['jurusan'];
        }
    }
    if (isset($_POST['nre'])){
        if($_POST['nre']!=''  || $_POST['nre']!=null) {
            $nre=$_POST['nre'];
        }
    }
    
    // versi code_spesifik

    // $teks = "INSERT INTO tbl_smsbroadcast_student (id_universitas,fakultas,jurusan,nre,time_window, content_fk,schedule,schedule_type,status,created_date,created_by,type,priority,sender, code_spesifik) VALUES ('".$_POST['id_universitas']."','".$_POST['fakultas']."','".$_POST['jurusan']."','".$_POST['nre']."','".$_POST['time_window']."','" . $_POST['sms_content'] . "','" . $schedule . "','" . $_POST['schedule_type'] . "','P','" . $date . "','" . $_GET['user'] . "','" . $sentTo . "','" . $_POST['priority'] . "','" . $_POST['sender'] . "','" . $_POST['code_spesifik'] . "')";

    // versi nre
    $teks = "INSERT INTO tbl_smsbroadcast_student (type,id_universitas,fakultas,jurusan,nre,time_window, sender,schedule,schedule_type,status,created_date,created_by,priority,message,code_spesifik) VALUES ('".$_POST['selectdata']."','".$_GET['id_universitas']."','".$fakultas."','".$jurusan."','".$nre."','".$_POST['time_window']."','" . $_GET['sender'] . "','" . $schedule . "','" . $_POST['schedule_type'] . "','P','" . $date . "','" . $_GET['user'] . "','" . $_POST['priority'] . "','" . $_POST['message'] . "','" . $_POST['code_spesifik'] . "')";


    $query  = $link->query($teks);

    if ($query) {
        $last_id = $link->insert_id;
        $result = array('success' => true, 'msg' => 'Success', 'query' => $teks, 'id' => $last_id);
    } else {
        $result = array('success' => false, 'msg' => 'Failed', 'query' => $teks);
    }

    echo json_encode($result);
} elseif ($action == 'update') {
    if($_POST['schedule_type']=='C'){
        $tamp = explode("T", $_POST['schedule']);
        $schedule = $tamp[0] . ' ' . $tamp[1] . ":00";
    }else{
        //$schedule = date('Y-m-d H:i:s');
        $schedule=date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." +20 minutes"));
        
    }

    $fakultas='NULL';
    $jurusan='NULL';
    // $nre='NULL';
    $message = 'NULL';
    $code_spesifik = 'NULL';
    $time_window = '24';
    $priority = '0';
    $selectdata = 'U';
    $schedule_type = $_POST['schedule_type'];


    if (isset($_POST['fakultas'])){
        if($_POST['fakultas']!=''  || $_POST['fakultas']!=null) {
            $fakultas=$_POST['fakultas'];
        }
    }


    if (isset($_POST['selectdata'])){
        if($_POST['selectdata']!=''  || $_POST['selectdata']!=null) {
            $selectdata=$_POST['selectdata'];
        }
    }

    if (isset($_POST['time_window'])){
        if($_POST['time_window']!=''  || $_POST['time_window']!=null) {
            $time_window=$_POST['time_window'];
        }
    }

    if (isset($_POST['message'])){
        if($_POST['message']!=''  || $_POST['message']!=null) {
            $message=$_POST['message'];
        }
    }

    if (isset($_POST['code_spesifik'])){
        if($_POST['code_spesifik']!=''  || $_POST['code_spesifik']!=null) {
            $code_spesifik=$_POST['code_spesifik'];
        }
    }

    if (isset($_POST['priority'])){
        if($_POST['priority']!=''  || $_POST['priority']!=null) {
            $priority=$_POST['priority'];
        }
    }

    if (isset($_POST['jurusan'])){
        if($_POST['jurusan']!=''  || $_POST['jurusan']!=null) {
            $jurusan=$_POST['jurusan'];
        }
    }
   
   if($_POST['schedule_type']=='C'){
        $tamp = explode("T", $_POST['schedule']);
        $schedule = $tamp[0] . ' ' . $tamp[1] . ":00";
    }else{
        //$schedule = date('Y-m-d H:i:s');
        $schedule=date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")." +20 minutes"));
        
    }
    // versi nre
    $teks = "UPDATE tbl_smsbroadcast_student 
             SET
             type              = '$selectdata',
             jurusan           = '$jurusan',
             fakultas          = '$fakultas',
             code_spesifik     = '$code_spesifik',
             time_window       = '$time_window',
             message           = '$message',
             schedule          = '$schedule',
             schedule_type    =  '$schedule_type'
             

             WHERE id= '".$_POST['id']."'";

            

    $query  = $link->query($teks);

    if ($query) {
        $result = array('success' => true, 'msg' => 'Success', 'query' => $teks);
    } else {
        $result = array('success' => false, 'msg' => 'Failed', 'query' => $teks);
    }

    echo json_encode($result);
} elseif ($action == 'uploadFile') {
    $data       = $_POST['data'];
    $fileName   = $_POST['name'];
    $serverFile = $fileName;

    list($type, $data) = explode(';', $data);
    list(, $data) = explode(',', $data);
    $data       = base64_decode($data);
    file_put_contents('../upload/' . $serverFile, $data);
    $returnData = array("serverFile" => $serverFile);
    echo json_encode($returnData);
} elseif ($action == 'updateFile') {
    $id        = $_POST['id'];
    $name      = $_POST['name'];
    $teks = "UPDATE tbl_smsbroadcast_student SET file = '$name' WHERE id=$id";
    $q         = $link->query($teks);
    if ($q) {
        $result = array('success' => true, 'msg' => 'Success update file');
    } else {
        $result = array('success' => false, 'msg' => 'Failed update file');
    }
    echo json_encode($result);
} elseif ($action == "delete") {
    $teks = "DELETE FROM tbl_smsbroadcast_student WHERE id=" . $_GET['id'];

    $query  = $link->query($teks);

    if ($query) {
        $result = array('success' => true, 'msg' => 'Success', 'query' => $teks);
    } else {
        $result = array('success' => false, 'msg' => 'Failed', 'query' => $teks);
    }

    echo json_encode($result);
} else if ($action == 'getBroadcast') {

    $id = $_GET['id'];

    // $teks = "SELECT * FROM tbl_smsbroadcast WHERE id = '$id'";
    // $teks = "SELECT b.*, c.name, c.tetun, c.bahasa, c.english FROM tbl_smsbroadcast_student b LEFT JOIN tbl_contents c ON b.content_fk = c.id WHERE b.id = $id;";

    $teks = "SELECT * FROM tbl_smsbroadcast_student b  WHERE b.id = $id;";



    $query  = $link->query($teks);

    if ($query) {
        $result = array('success' => true, 'data' => array(), 'msg' => 'Success', 'query' => $teks);
        while ($row = $query->fetch_assoc()) {
            array_push($result['data'], $row);
        }
    } else {
        $result = array('success' => false, 'msg' => 'Failed', 'query' => $teks);
    }

    echo json_encode($result);
} elseif ($action == 'terminate') {
    $id        = $_GET['id'];
    $teks = "UPDATE tbl_smsbroadcast_student SET status = 'T' WHERE id=$id";
    $q         = $link->query($teks);
    if ($q) {
        $result = array('success' => true, 'msg' => 'Success Terminate');
    } else {
        $result = array('success' => false, 'msg' => 'Failed Terminate');
    }
    echo json_encode($result);
}

mysqli_close($link);
