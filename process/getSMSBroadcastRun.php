<?php
include "connection.php";
date_default_timezone_set('asia/jayapura');
header('Access-Control-Allow-Origin:*');
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

$columns = array("No","Sender","Content","Schedule","Total SMS","Submit SMS","Message In Queue"/*,"Delivered SMS","Undelivered SMS","Estimated SMS","Status","Windows Time","Action"*/); // data array columns harus sama dengan data header table yang tadi di buat di view

$start = $_GET["start"]; //0
$length = $_GET["length"]; //10
$cari_data = $_GET["search"];
$cari_data = $cari_data['value'];


if (empty($cari_data)) {
    $dt = $link->query("SELECT a.*,b.name FROM tbl_smsbroadcast_student a LEFT JOIN tbl_contents b ON b.id=a.content_fk WHERE a.status='R' ORDER BY a.created_date DESC LIMIT $start,$length");
    $total = $link->query("SELECT COUNT(a.id) as total FROM tbl_smsbroadcast_student a LEFT JOIN tbl_contents b ON b.id=a.content_fk WHERE a.status='R'");
} else {
    $dt = $link->query("SELECT a.*,b.name 
                            FROM tbl_smsbroadcast_student a 
                            LEFT JOIN tbl_contents b ON b.id=a.content_fk
                            WHERE a.status='R' AND a.sender LIKE '%$cari_data%'
                            OR b.name LIKE '%$cari_data%'
                            OR a.schedule LIKE '%$cari_data%'
                            ORDER BY a.created_date DESC LIMIT $start,$length");
    $total = $link->query("SELECT COUNT(a.id) as total
                               FROM tbl_smsbroadcast_student a 
                               LEFT JOIN tbl_contents b ON b.id=a.content_fk
                               WHERE a.status='R' AND a.sender LIKE '%$cari_data%'
                               OR b.name LIKE '%$cari_data%'
                               OR a.schedule LIKE '%$cari_data%'
                               ORDER BY a.created_date DESC LIMIT $start,$length");
}
$count = mysqli_fetch_array($total, MYSQLI_ASSOC);

// var_dump($count);die();


$array = array();


while ($row = mysqli_fetch_array($dt, MYSQLI_ASSOC)) {
    array_push($array, $row);
}


$array_data = array();

//print_r($array);die();

function status($status)
{
    switch ($status) {
        case 'P':
            return "Pending";
            break;
        case 'A':
            return "Approvel";
            break;

        case 'S':
            return "Send";
            break;

        case 'D':
            return "Done";
            break;
        case 'E':
            return "Error";
            break;
        case 'R':
            return "Running";
            break;
        case 'T':
            return "Terminate";
            break;
    }
}

function showButton($status, $id)
{
    if ($status == "P") {
        return "<button type='button' class='btn btn-success mr-2' onclick=\"edit('" . $id . "')\" data-toggle=\"modal\" data-target=\"#myModal\">Edit</button><button type='button' class='btn btn-danger mr-2' onclick=\"delBroadcast('" . $id . "')\">Delete</button>";
    } else if ($status == "R") {
        return "<button type='button' class='btn btn-warning mr-2' onclick=\"terminate('" . $id . "')\" >Terminate</button>";
    }else{
        return "";
    }
}

$no = $start + 1;
for ($i = 0; $i < count($array); $i++) {

    $data["No"] = $no;
    $data["Content"] = $array[$i]['name'];
    $data["Sender"] = $array[$i]['sender'];
    $data["Schedule"] = $array[$i]['schedule'];
    $data["Total SMS"] = $array[$i]['total_msisdn'];
    $data["Submit SMS"] = $array[$i]['sms_sm'];
    $data["Message In Queue"] = 0;

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_PORT           => "8080",
        CURLOPT_URL            => "http://".$_SERVER['HTTP_HOST'].'/smsbc/getMessageInQueue?messageId='.$array[$i]['id'],
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING       => "",
        CURLOPT_MAXREDIRS      => 10,
        CURLOPT_TIMEOUT        => 30,
        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST  => "GET",
        CURLOPT_HTTPHEADER     => array(
            "cache-control: no-cache"
        ),
    ));

    $response = curl_exec($curl);
    $err      = curl_error($curl);

    curl_close($curl);

    // $ch = curl_init(); 

    // // set url 
    // curl_setopt($ch, CURLOPT_URL, 'http://172.20.212.16:8080/smsbc/getMessageInQueue?messageId='.$array[$i]['id']);

    // // return the transfer as a string 
    // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

    // // $output contains the output string 
    // $output = curl_exec($ch); 

    // // tutup curl 
    // curl_close($ch);      

    // menampilkan hasil curl
    $data["Message In Queue"] = $response;

    // $data["Delivered SMS"] = $array[$i]['sms_dr'];
    // $data["Status"] = status($array[$i]['status']);
    // $data["Action"] = showButton($array[$i]['status'], $array[$i]['id']);
    // $data["Created Date"] = $array[$i]['created_date'];
    // $data["Check"] = "";


    array_push($array_data, $data);
    $no++;
}
// foreach (mysqli_fetch_array($dt,MYSQLI_ASSOC) as $key) {

//     $data["No"]=$no;
//     $data["Destination Number"]=$key->destination_number;
//     $data["Origin Number"]=$key->origin_number;

//     array_push($array_data,$data);
//     $no++;

// }


//settingan terpenting dari datatable

$output = array(

    "draw" => intval($_GET["draw"]),
    "recordsTotal" => intval($count['total']),
    "recordsFiltered" => intval($count['total']),
    "data" => $array_data
);

echo json_encode($output);
//}
mysqli_close($link);
