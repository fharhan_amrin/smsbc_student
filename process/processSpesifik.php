<?php

if (function_exists($_GET['f'])) {
    $_GET['f']();
}

function insertDataSpesifik()
{
    include "connection.php";

    $dateTime = date("Y-m-d h:i:s");

    $code_spesifik = $_POST['code_spesifik'];
    $desc = $_POST['desc'];
    $spesifik = $_POST['spesifik'];

    $arr = json_decode($spesifik, true);

    if (array_key_exists('MSISDN', $arr[0])) {
        $q = "insert into tbl_spesifik (code_spesifik, description, created_date) values ('DB-$code_spesifik', '$desc', '$dateTime')";
        $query = $link->query($q); 

        if ($query) {
            foreach ($arr as $row) {
                $q2 = "insert into tbl_msisdn_spesifik (code_spesifik, msisdn) values ('DB-$code_spesifik', '" . $row['MSISDN'] . "')";
                $link->query($q2);
            }
            $result = array('success' => true, 'msg' => 'Success');
        } else {
            $result = array('success' => false, 'msg' => 'Code Exclude already exists');
        }
    } else {
        $result = array('success' => false, 'msg' => 'Data exclude doesn\'t match');
    }



    // if ($query) {
    //     foreach ($arr as $row) {

    //         if (array_key_exists('MSISDN', $row)) {
    //             

    //             if ($query2) {
    //                 $result = array('success' => true, 'msg' => 'Success');
    //             } else {
    //                 $result = array('success' => true, 'msg' => 'Failed');
    //             }
    //         }else{
    //             $result = array('success' => true, 'msg' => 'Data doesn\'t match');
    //         }
    //     }
    // } else {
    //     $result = array('success' => false, 'msg' => 'Code Exclude already exists');
    // }

    echo json_encode($result);
    mysqli_close($link);
}

function getData()
{

    include "connection.php";

    if(isset($_GET['search'])){
        $teks = "select * from tbl_spesifik where code_spesifik like '%".$_GET['search']."%';";
    }else{
        $teks = "select * from tbl_spesifik;";
    }

    $query = $link->query($teks);

    if ($query->num_rows > 0) {
        $list = array();
        $key = 0;
        while ($row = $query->fetch_assoc()) {
            $list[$key]['id'] = $row['code_spesifik'];
            $list[$key]['text'] = $row['code_spesifik'];
            $key++;
        }
        echo json_encode($list);
    } else {
        echo "hasil kosong";
    }
}



function AddMsisdn()
{
    include "connection.php";

    $codeEx = $_POST['code_spesifik'];
    $msisdn = $_POST['msisdn'];

    $q = "INSERT INTO tbl_msisdn_spesifik (code_spesifik, msisdn) VALUES ('$codeEx', '$msisdn')";
    $query = $link->query($q); 

    if ($query) {
        $result = array('success' => true, 'msg' => 'Success', 'msql'=>$q);
    } else {
        $result = array('success' => false, 'msg' => 'Error', 'msql'=>$q);
    }

    mysqli_close($link);
    echo json_encode($result);
}

function DeleteMsisdn()
{
    include "connection.php";

    $id = $_GET['id'];

    $q = "DELETE FROM tbl_msisdn_spesifik WHERE id=$id";
    $query = $link->query($q); 

    if ($query) {
        $result = array('success' => true, 'msg' => 'Success', 'msql'=>$q);
    } else {
        $result = array('success' => false, 'msg' => 'Error', 'msql'=>$q);
    }

    echo json_encode($result);
    mysqli_close($link);
}