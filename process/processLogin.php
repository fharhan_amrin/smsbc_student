<?php

if (function_exists($_GET['f'])) {
    $_GET['f']();
}

function Login()
{
    include "connection.php";

    $user = $_POST['username'];
    $pass = $_POST['password'];

    $q = "SELECT * FROM user WHERE username='$user' and password='$pass'";
    $query = $link->query($q); 

    $data = $query->fetch_assoc();

    if ($query) {
        $result = array('success' => true, 'msg' => 'Success', 'msql'=>$q, 'data'=>$data);
    } else {
        $result = array('success' => false, 'msg' => 'Error', 'msql'=>$q);
    }

    echo json_encode($result);
    mysqli_close($link);
}

function getUser()
{
    include "connection.php";

    $start = $_GET["start"]; //0
    $length = $_GET["length"]; //10
    $search = $_GET['search'];
    $search = $search['value'];

    if (empty($search)) {
        $query = $link->query("select * from user limit $start,$length;");
        $total = $link->query("select  count(id) as total from user;");
    } else {
        $query = $link->query("select * from user where sender_name like '%$search%' OR username like '%$search%' limit $start,$length;");
        $total = $link->query("select  count(id) as total from user where sender_name like '%$search%' OR username like '%$search%';");
    }

    $count = mysqli_fetch_array($total, MYSQLI_ASSOC);

    $array = array();
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        array_push($array, $row);
    }

    $array_data = array();
    $no = $start + 1;
    for ($i = 0; $i < count($array); $i++) {

        $data["No"] = $no;
        $data["Sender Name"] = $array[$i]['sender_name'];
        $data["Username"] = $array[$i]['username'];
        $data["University"] = getUniversitasbyId($array[$i]['id_universitas']);
        $data["Action"] = "<button type='button' class='btn btn-primary mr-2' onclick=\"edit_user('" . $array[$i]['id'] . "')\" data-toggle=\"modal\" data-target=\"#myModalEdit\">Edit</button>
                           <button type='button' class='btn btn-danger mr-2' onclick=\"delete_user('" . $array[$i]['id'] . "')\" >Delete</button>";


        array_push($array_data, $data);
        $no++;
    }

    $output = array(
        "draw" => intval($_GET["draw"]),
        "recordsTotal" => intval($count['total']),
        "recordsFiltered" => intval($count['total']),
        "data" => $array_data
    );


    echo json_encode($output);
    mysqli_close($link);
}

function AddUser()
{
    include "connection.php";

    $name = $_POST['name'];
    $user = $_POST['username'];
    $pass = md5($_POST['password']);
    $text = $_POST['password'];
    $univ = $_POST['univ'];

    $q = "INSERT INTO user (sender_name, username, password, text_password, id_universitas) VALUES ('$name', '$user', '$pass','$text','$univ')";
    $query = $link->query($q); 

    if ($query) {
        $result = array('success' => true, 'msg' => 'Success', 'msql'=>$q);
    } else {
        $result = array('success' => false, 'msg' => 'Error', 'msql'=>$q);
    }

    mysqli_close($link);
    echo json_encode($result);
}


function EditUser()
{
    include "connection.php";

    $id = $_POST['id'];
    $name = $_POST['e_name'];
    $user = $_POST['e_username'];
    $pass = md5($_POST['e_password']);
    $text = $_POST['e_password'];
    $univ = '';

    if($_POST['e_univ']!=''){
        $univ=$_POST['e_univ'];
    }else{
        $univ=$_POST['e_oldunivid'];
    }

    $q = "REPLACE INTO user (id,sender_name, username, password, text_password,id_universitas) VALUES ('$id','$name', '$user', '$pass','$text','$univ')";
    $query = $link->query($q); 

    if ($query) {
        $result = array('success' => true, 'msg' => 'Success', 'msql'=>$q);
    } else {
        $result = array('success' => false, 'msg' => 'Error', 'msql'=>$q);
    }

    mysqli_close($link);
    echo json_encode($result);
}

function UserId()
{
    include "connection.php";

    $id = $_GET['id'];

    $q = "SELECT * FROM user WHERE id='$id';";
    $query = $link->query($q); 

    $data = $query->fetch_assoc();
    $data['university'] = getUniversitasbyId($data['id_universitas']);

    if ($query) {
        $result = array('success' => true, 'msg' => 'Success', 'msql'=>$q, 'data'=>$data);
    } else {
        $result = array('success' => false, 'msg' => 'Error', 'msql'=>$q);
    }

    echo json_encode($result);
    mysqli_close($link);
}

function DeleteUser()
{
    include "connection.php";

    $id = $_GET['id'];

    $q = "DELETE FROM user WHERE id=$id";
    $query = $link->query($q); 

    if ($query) {
        $result = array('success' => true, 'msg' => 'Success', 'msql'=>$q);
    } else {
        $result = array('success' => false, 'msg' => 'Error', 'msql'=>$q);
    }

    echo json_encode($result);
    mysqli_close($link);
}



function getUniversitasbyId($id)
{

    include "connection235.php";
    $q = "SELECT * FROM students_university WHERE iduniversity='$id';";
    $query = $link235->query($q); 

    $data = $query->fetch_assoc();

    return $data['name'];
}

function getUniv()
{

    include "connection235.php";

    $search = $_GET['search'];

    $teks = "SELECT iduniversity as id,name FROM students_university WHERE name LIKE '%$search%'";

    $query  = $link235->query($teks);

    if ($query->num_rows > 0) {
        $list = array();
        $key = 0;
        while ($row = $query->fetch_assoc()) {
            $list[$key]['id'] = $row['id'];
            $list[$key]['text'] = $row['name'];
            $key++;
        }
        echo json_encode($list);
    } else {
        echo "Not Found";
    }
}

function getFakultas()
{

    include "connection235.php";

    $search = $_GET['search'];

    $teks = "SELECT idfaculty as id,name FROM students_faculty WHERE name LIKE '%$search%'";

    $query  = $link235->query($teks);

    if ($query->num_rows > 0) {
        $list = array();
        $key = 0;
        while ($row = $query->fetch_assoc()) {
            $list[$key]['id'] = $row['id'];
            $list[$key]['text'] = $row['name'];
            $key++;
        }
        echo json_encode($list);
    } else {
        echo "Not Found";
    }
}

function getFaculty()
{

    include "connection235.php";

    $id=$_GET['iduniv'];

    $teks = "SELECT students_faculty.idfaculty,students_faculty.name FROM students_regist_new2 INNER JOIN students_faculty ON students_regist_new2.fakultas = students_faculty.idfaculty where university='$id'  group by fakultas";

    $query  = $link235->query($teks);

    $data = array();

    //$data = $query->fetch_assoc();

    $key = 0;
    while ($row = $query->fetch_assoc()) {
        $data[$key]['idfaculty'] = $row['idfaculty'];
        $data[$key]['name'] = $row['name'];
        $key++;
    }

    if ($query) {
        $result = array('success' => true, 'data' => $data);
    } else {
        $result = array('success' => false, 'msg' => 'Failed to fetch all data ');
    }
    
    echo json_encode($result);
}

function getJurusan()
{

    include "connection235.php";

    $id=$_GET['id'];
    $iduniv=$_GET['iduniv'];

    // echo $id;

    $teks = "SELECT  distinct(jurusan),university,fakultas FROM students_regist_new2  where university='$iduniv' and fakultas in ($id)";

    // echo $teks;

    // $query  = $link235->query($teks);
    $result = $link235->query($teks);
        if($result){

            if ($result->num_rows > 0) {
                // output data of each row
                $array = array('success'=>true, 'data'=>array());
                // print_r($array['data']);
                
                while($row = $result->fetch_assoc()){
                    array_push($array['data'], $row);
                }
            } else {
                $array = array('success'=>false, 'data'=>array(),'msg'=>'Cannot fetch data from database');
            }
        }else{
            $array = array('success'=>false, 'data'=>array(),'msg'=>'Cannot fetch data from database. Query Error');

        }
        $array['qDebug'] = $teks;

        echo json_encode($array);
}


function getNreSpesifik()
{

    include "connection.php";



    $teks = "SELECT * FROM `tbl_spesifik`";

    // echo $teks;

    $query  = $link->query($teks);

    $data = array();

    //$data = $query->fetch_assoc();

    $key = 0;
    while ($row = $query->fetch_assoc()) {
        $data[$key]['code_spesifik'] = $row['code_spesifik'];
        $key++;
    }

    if ($query) {
        $result = array('success' => true, 'data' => $data);
    } else {
        $result = array('success' => false, 'msg' => 'Failed to fetch all data ');
    }
    
    echo json_encode($result);
}
