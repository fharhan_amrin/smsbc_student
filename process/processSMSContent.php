<?php
include "connection.php";

date_default_timezone_set('asia/jayapura');
header('Access-Control-Allow-Origin:*');
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');


$action = @$_GET['action'];
$date = date('Y-m-d H:i:s');
if ($action == 'insert') {

    $teks = "INSERT INTO tbl_contents (sender,name,tetun,english,bahasa,created_date,created_by) VALUES ('" . $_POST['sender'] . "','" . $_POST['name'] . "','" . $_POST['tetun'] . "','" . $_POST['english'] . "','" . $_POST['bahasa'] . "','" . $date . "','" . $_GET['user'] . "')";

    $query  = $link->query($teks);

    if ($query) {
        $result = array('success' => true, 'msg' => 'Success', 'query' => $teks);
    } else {
        $result = array('success' => false, 'msg' => 'Failed', 'query' => $teks);
    }

    echo json_encode($result);
} else if ($action == 'getdata') {

    $search = $_GET['search'];

    $teks = "SELECT id,name FROM tbl_contents WHERE name LIKE '%$search%'";

    $query  = $link->query($teks);

    if ($query->num_rows > 0) {
        $list = array();
        $key = 0;
        while ($row = $query->fetch_assoc()) {
            $list[$key]['id'] = $row['id'];
            $list[$key]['text'] = $row['name'];
            $key++;
        }
        echo json_encode($list);
    } else {
        echo "hasil kosong";
    }
} else if ($action == 'getlanguage') {

    $search = $_GET['id'];

    $teks = "SELECT * FROM tbl_contents WHERE id = '$search'";

    $query  = $link->query($teks);

    if ($query) {
        $result = array('success' => true, 'data' => array(), 'msg' => 'Success', 'query' => $teks);
        while ($row = $query->fetch_assoc()) {
            array_push($result['data'], $row);
        }
    } else {
        $result = array('success' => false, 'msg' => 'Failed', 'query' => $teks);
    }

    echo json_encode($result);
} else if ($action == 'getContent') {

    $search = $_GET['id'];

    $teks = "SELECT * FROM tbl_contents WHERE id = '$search'";

    $query  = $link->query($teks);

    if ($query) {
        $result = array('success' => true, 'data' => array(), 'msg' => 'Success', 'query' => $teks);
        while ($row = $query->fetch_assoc()) {
            array_push($result['data'], $row);
        }
    } else {
        $result = array('success' => false, 'msg' => 'Failed', 'query' => $teks);
    }

    echo json_encode($result);
} else if ($action == 'update') {

    $teks = "REPLACE INTO tbl_contents (id,sender,name,tetun,english,bahasa,created_date,created_by) VALUES ('" . $_POST['id'] . "','" . $_POST['sender'] . "','" . $_POST['name'] . "','" . $_POST['tetun'] . "','" . $_POST['english'] . "','" . $_POST['bahasa'] . "','" . $date . "','" . $_GET['user'] . "')";

    $query  = $link->query($teks);

    if ($query) {
        $result = array('success' => true, 'msg' => 'Success', 'query' => $teks);
    } else {
        $result = array('success' => false, 'msg' => 'Failed', 'query' => $teks);
    }

    echo json_encode($result);
} else if ($action == 'delete') {

    $teks = "DELETE FROM tbl_contents WHERE id=" . $_GET['id'];

    $query  = $link->query($teks);

    if ($query) {
        $result = array('success' => true, 'msg' => 'Success', 'query' => $teks);
    } else {
        $result = array('success' => false, 'msg' => 'Failed', 'query' => $teks);
    }

    echo json_encode($result);
}

mysqli_close($link);
