<?php

if (function_exists($_GET['f'])) {
    $_GET['f']();
}

function getSpesifik()
{
    include "connection.php";

    $start = $_GET["start"]; //0
    $length = $_GET["length"]; //10
    $search = $_GET['search'];
    $search = $search['value'];

    if (empty($search)) {
        $query = $link->query("select  cx.code_spesifik, count(mx.msisdn) as total,  cx.created_date from tbl_msisdn_spesifik mx left join tbl_spesifik cx on mx.code_spesifik = cx.code_spesifik group by mx.code_spesifik order by cx.code_spesifik desc  limit $start,$length;");
    } else {
        $query = $link->query("select  cx.code_spesifik, count(mx.msisdn) as total,  cx.created_date from tbl_msisdn_spesifik mx left join tbl_spesifik cx on mx.code_spesifik = cx.code_spesifik where cx.code_spesifik like '%$search%'  group by mx.code_spesifik order by cx.code_spesifik desc  limit $start,$length;");
    }

    $array = array();
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        array_push($array, $row);
    }

    $array_data = array();
    $no = $start + 1;
    for ($i = 0; $i < count($array); $i++) {

        $data["No"] = $no;
        $data["Code Specific"] = $array[$i]['code_spesifik'];
        $data["Total Msisdn"] = $array[$i]['total'];
        $data["Created Date"] = $array[$i]['created_date'];
        $data["Action"] = "<button type='button' class='btn btn-primary mr-2' onclick=\"detail('" . $array[$i]['code_spesifik'] . "')\" data-toggle=\"modal\" data-target=\"#myModal\">Detail</button>
                           <button type='button' class='btn btn-success mr-2' onclick=\"add_msisdn('" . $array[$i]['code_spesifik'] . "')\" data-toggle=\"modal\" data-target=\"#myModalAdd\">Add Msisdn</button>";

        array_push($array_data, $data);
        $no++;
    }

    $output = array(
        "draw" => intval($_GET["draw"]),
        "recordsTotal" => intval(count($array_data)),
        "recordsFiltered" => intval(count($array_data)),
        "data" => $array_data
    );


    echo json_encode($output);
    mysqli_close($link);
}



function getExcludeDetail()
{
    include "connection.php";

    $start = $_GET["start"]; //0
    $length = $_GET["length"]; //10
    $search = $_GET['search'];
    $search = $search['value'];

    if (empty($search)) {
        $query = $link->query("select * from tbl_msisdn_spesifik where code_spesifik='".$_GET['id']."' limit $start,$length;");
        $total = $link->query("select count(id) as total from tbl_msisdn_spesifik where code_spesifik='".$_GET['id']."';");
    } else {
        $query = $link->query("select * from tbl_msisdn_spesifik where code_spesifik='".$_GET['id']."' and msisdn like '%$search%' limit $start,$length;");
        $total = $link->query("select count(id) as total from tbl_msisdn_spesifik where code_spesifik='".$_GET['id']."' and msisdn like '%$search%';");
    }

    $count = mysqli_fetch_array($total, MYSQLI_ASSOC);

    $array = array();
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        array_push($array, $row);
    }

    $array_data = array();
    $no = $start + 1;
    for ($i = 0; $i < count($array); $i++) {

        $data["No"] = $no;
        $data["MSISDN"] = $array[$i]['msisdn'];
        $data["Action"] = "<button type='button' class='btn btn-danger mr-2' onclick=\"delete_msisdn('" . $array[$i]['id'] . "')\" >Delete</button>";


        array_push($array_data, $data);
        $no++;
    }

    $output = array(
        "draw" => intval($_GET["draw"]),
        "recordsTotal" => intval($count['total']),
        "recordsFiltered" => intval($count['total']),
        "data" => $array_data
    );


    echo json_encode($output);
    mysqli_close($link);
}
