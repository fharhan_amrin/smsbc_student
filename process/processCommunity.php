<?php

if (function_exists($_GET['f'])) {
    $_GET['f']();
}

function getCom()
{
    include "connection.php";

    $start = $_GET["start"]; //0
    $length = $_GET["length"]; //10
    $search = $_GET['search'];
    $search = $search['value'];

    if (empty($search)) {
        $query = $link->query("select * from tbl_community limit $start,$length;");
        $total = $link->query("select  count(communityID) as total from tbl_community;");
    } else {
        $query = $link->query("select * from tbl_community where description like '%$search%' limit $start,$length;");
        $total = $link->query("select  count(communityID) as total from tbl_community where description like '%$search%';");
    }

    $count = mysqli_fetch_array($total, MYSQLI_ASSOC);

    $array = array();
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        array_push($array, $row);
    }

    $array_data = array();
    $no = $start + 1;
    for ($i = 0; $i < count($array); $i++) {

        $data["No"] = $no;
        $data["Community"] = $array[$i]['description'];
        $data["Action"] = "<button type='button' class='btn btn-primary mr-2' onclick=\"edit_com('" . $array[$i]['communityID'] . "')\" data-toggle=\"modal\" data-target=\"#myModalEdit\">Edit</button>
                           <button type='button' class='btn btn-danger mr-2' onclick=\"delete_com('" . $array[$i]['communityID'] . "')\" >Delete</button>";


        array_push($array_data, $data);
        $no++;
    }

    $output = array(
        "draw" => intval($_GET["draw"]),
        "recordsTotal" => intval($count['total']),
        "recordsFiltered" => intval($count['total']),
        "data" => $array_data
    );


    echo json_encode($output);
    mysqli_close($link);
}

function AddCom()
{
    include "connection.php";

    $description = $_POST['com'];

    $q = "INSERT INTO tbl_community (description) VALUES ('$description')";
    $query = $link->query($q); 

    if ($query) {
        $result = array('success' => true, 'msg' => 'Success', 'msql'=>$q);
    } else {
        $result = array('success' => false, 'msg' => 'Error', 'msql'=>$q);
    }

    mysqli_close($link);
    echo json_encode($result);
}


function EditCom()
{
    include "connection.php";

    $id = $_POST['id'];
    $description = $_POST['e_com'];

    $q = "REPLACE INTO tbl_community (communityID,description) VALUES ('$id','$description')";
    $query = $link->query($q); 

    if ($query) {
        $result = array('success' => true, 'msg' => 'Success', 'msql'=>$q);
    } else {
        $result = array('success' => false, 'msg' => 'Error', 'msql'=>$q);
    }

    mysqli_close($link);
    echo json_encode($result);
}

function ComId()
{
    include "connection.php";

    $id = $_GET['id'];

    $q = "SELECT * FROM tbl_community WHERE communityID='$id';";
    $query = $link->query($q); 

    $data = $query->fetch_assoc();

    if ($query) {
        $result = array('success' => true, 'msg' => 'Success', 'msql'=>$q, 'data'=>$data);
    } else {
        $result = array('success' => false, 'msg' => 'Error', 'msql'=>$q);
    }

    echo json_encode($result);
    mysqli_close($link);
}

function DeleteCom()
{
    include "connection.php";

    $id = $_GET['id'];

    $q = "DELETE FROM tbl_community WHERE communityID=$id";
    $query = $link->query($q); 

    if ($query) {
        $result = array('success' => true, 'msg' => 'Success', 'msql'=>$q);
    } else {
        $result = array('success' => false, 'msg' => 'Error', 'msql'=>$q);
    }

    echo json_encode($result);
    mysqli_close($link);
}

