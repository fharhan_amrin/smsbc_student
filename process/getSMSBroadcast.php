<?php
include "connection.php";
include "connection235.php";
date_default_timezone_set('asia/jayapura');
header('Access-Control-Allow-Origin:*');
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

$columns = array("No", "fakultas", "jurusan", "Content", "Schedule", "Total SMS", "Submit SMS", "Delivered SMS", "Undelivered SMS", "Submit Start", "Submit End", "Status", "Window Time", "Action"); // data array columns harus sama dengan data header table yang tadi di buat di view

$start = $_POST["start"]; //0
$length = $_POST["length"]; //10
$cari_data = $_POST["search"];
$cari_data = $cari_data['value'];
$userSession = $_GET['userSession'];


if (empty($cari_data)) {

    $dt = $link->query("SELECT a.*,b.name FROM tbl_smsbroadcast_student a LEFT JOIN tbl_contents b ON b.id=a.content_fk WHERE a.created_by='$userSession' ORDER BY a.created_date DESC LIMIT $start,$length");

    $total = $link->query("SELECT COUNT(a.id) as total FROM tbl_smsbroadcast_student a LEFT JOIN tbl_contents b ON b.id=a.content_fk WHERE a.created_by='$userSession'");
} else {
    $dt = $link->query("SELECT a.*,b.name 
                            FROM tbl_smsbroadcast_student a 
                            LEFT JOIN tbl_contents b ON b.id=a.content_fk
                            WHERE a.sender LIKE '%$cari_data%'
                            OR b.name LIKE '%$cari_data%'
                            OR a.schedule LIKE '%$cari_data%'
                            ORDER BY a.created_date DESC LIMIT $start,$length");
    $total = $link->query("SELECT COUNT(a.id) as total
                               FROM tbl_smsbroadcast_student a 
                               LEFT JOIN tbl_contents b ON b.id=a.content_fk
                               WHERE a.sender LIKE '%$cari_data%'
                               OR b.name LIKE '%$cari_data%'
                               OR a.schedule LIKE '%$cari_data%'

                               ORDER BY a.created_date DESC LIMIT $start,$length");
}
$count = mysqli_fetch_array($total, MYSQLI_ASSOC);

// var_dump($count);die();


$array = array();


while ($row = mysqli_fetch_array($dt, MYSQLI_ASSOC)) {
    array_push($array, $row);
}


$array_data = array();

//print_r($array);die();/

function status($status)
{
    switch ($status) {
        case 'P':
            return "Pending";
            break;
        case 'A':
            return "Approvel";
            break;

        case 'S':
            return "Send";
            break;

        case 'D':
            return "Done";
            break;
        case 'E':
            return "Error";
            break;
        case 'R':
            return "Running";
            break;
        case 'T':
            return "Terminate";
            break;
    }
}


function showButton($status, $id)
{
    if ($status == "P") {
        // return "<button type='button' class='btn btn-success mr-2' onclick=\"edit('" . $id . "')\" data-toggle=\"modal\" data-target=\"#myModal\">Edit</button><button type='button' class='btn btn-danger mr-2' onclick=\"delBroadcast('" . $id . "')\">Delete</button>";

        return "<button type='button' class='btn btn-success mr-2' onclick=\"editHalaman('" . $id . "')\" >Edit</button><button type='button' class='btn btn-danger mr-2' onclick=\"delBroadcast('" . $id . "')\">Delete</button>";
    } else if ($status == "R") {
        return "<button type='button' class='btn btn-warning mr-2' onclick=\"terminate('" . $id . "')\" >Terminate</button>";
    } else if ($status == "D") {
        return "<button type='button' class='btn btn-primary mr-2' onclick=\"detail_bro('" . $id . "')\" data-toggle=\"modal\" data-target=\"#myModal2\">Detail</button>";
    } else {
        return "";
    }
}

function getFakultasbyId($id)
{

    include "connection235.php";
    $q = "SELECT * FROM students_faculty WHERE idfaculty='$id';";
    $query = $link235->query($q);

    $data = $query->fetch_assoc();

    return $data['name'];
}
//echo json_encode($array);
$no = $start + 1;
for ($i = 0; $i < count($array); $i++) {
    $fakultas = '-';
    $jurusan = '-';
    if ($array[$i]['type'] == 'F') {
        $fakultas = getFakultasbyId($array[$i]['fakultas']);
    }


    if ($array[$i]['type'] == 'J') {
        $jurusan = $array[$i]['jurusan'];
        $fakultas = getFakultasbyId($array[$i]['fakultas']);
    }

    $data["No"] = $no;
    $data["fakultas"] = $fakultas;
    $data["jurusan"] = $jurusan;
    $data["Content"] = $array[$i]['name'];
    $data["Schedule"] = $array[$i]['schedule'];
    $data["Total SMS"] = $array[$i]['total_msisdn'];
    $data["Submit SMS"] = $array[$i]['sms_sm'];
    $data["Delivered SMS"] = $array[$i]['sms_dr'];
    $data["Undelivered SMS"] = $array[$i]['sms_undelivery'];
    $data["Submit Start"] = $array[$i]['submit_staring'];
    $data["Submit End"] = $array[$i]['submit_done'];
    $data["Status"] = status($array[$i]['status']);
    $data["Window Time"] = $array[$i]['time_window'] . '/hour';
    $data["Action"] = showButton($array[$i]['status'], $array[$i]['id']);
    // $data["Created Date"] = $array[$i]['created_date'];
    // $data["Check"] = "";


    array_push($array_data, $data);
    $no++;
}
// foreach (mysqli_fetch_array($dt,MYSQLI_ASSOC) as $key) {

//     $data["No"]=$no;
//     $data["Destination Number"]=$key->destination_number;
//     $data["Origin Number"]=$key->origin_number;

//     array_push($array_data,$data);
//     $no++;

// }


//settingan terpenting dari datatable

$output = array(

    "draw" => intval($_POST["draw"]),
    "recordsTotal" => intval($count['total']),
    "recordsFiltered" => intval($count['total']),
    "data" => $array_data
);

echo json_encode($output);
//}
mysqli_close($link);
