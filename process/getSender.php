<?php
date_default_timezone_set('asia/jakarta');
header('Access-Control-Allow-Origin:*');
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');
$servername = "localhost:8080";
$username = "root";
$password = "";
$dbname = "db_smsbc";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 

    $columns = array("No","Sender"/*,"Tetun","English","Bahasa","Created Date","Action"*/); // data array columns harus sama dengan data header table yang tadi di buat di view

    $start = $_POST["start"];//0
    $length = $_POST["length"];//10
    $cari_data = $_POST["search"];
    $cari_data = $cari_data['value'];
    
    
    if(empty($cari_data)){
        $dt = $conn->query("SELECT * FROM tbl_sender LIMIT $start,$length");
        $total = $conn->query("SELECT COUNT(id) as total FROM tbl_sender");
    }else{
        $dt = $conn->query("SELECT * FROM tbl_sender
                            WHERE sender LIKE '%$cari_data%' LIMIT $start,$length");
        $total = $conn->query("SELECT COUNT(id) as total FROM tbl_sender
                            WHERE sender LIKE '%$cari_data%'");
    }
    $count = $total->fetch_array();

    //var_dump($count);die();

    $array =array();

    while($row = $dt->fetch_assoc()){
        array_push($array, $row);
    }

    $array_data =array();

    //print_r($array);die();

    $no=$start+1;
    for ($i=0; $i < count($array); $i++) { 
        $data["No"]=$no;
        $data["Sender"]=$array[$i]['sender'];
        // $data["Tetun"]=$array[$i]['tetun'];
        // $data["English"]=$array[$i]['english'];
        // $data["Bahasa"]=$array[$i]['bahasa'];
        // $data["Created Date"]=$array[$i]['created_date'];
        // $data["Action"]="";

        array_push($array_data,$data);
        $no++;
    }
    // foreach ($dt->fetch_assoc() as $key) {
        
    //     $data["No"]=$no;
    //     $data["Destination Number"]=$key->destination_number;
    //     $data["Origin Number"]=$key->origin_number;

    //     array_push($array_data,$data);
    //     $no++;

    // }


    //settingan terpenting dari datatable

    $output=array(

        "draw"=>intval($_POST["draw"]),
        "recordsTotal"=>intval($count['total']),
        "recordsFiltered"=>intval($count['total']),
        "data"=>$array_data
    );

    echo json_encode($output);
//}
$conn->close();
?>
