<?php

if (function_exists($_GET['f'])) {
    $_GET['f']();
}

function getLoc()
{
    include "connection.php";

    $start = $_GET["start"]; //0
    $length = $_GET["length"]; //10
    $search = $_GET['search'];
    $search = $search['value'];

    if (empty($search)) {
        $query = $link->query("select * from tbl_location limit $start,$length;");
        $total = $link->query("select  count(pk) as total from tbl_location;");
    } else {
        $query = $link->query("select * from tbl_location where pk like '%$search%' or district like '%$search%' limit $start,$length;");
        $total = $link->query("select  count(pk) as total from tbl_location where pk like '%$search%' or district like '%$search%';");
    }

    $count = mysqli_fetch_array($total, MYSQLI_ASSOC);

    $array = array();
    while ($row = mysqli_fetch_array($query, MYSQLI_ASSOC)) {
        array_push($array, $row);
    }

    $array_data = array();
    $no = $start + 1;
    for ($i = 0; $i < count($array); $i++) {

        $data["No"] = $no;
        $data["Location ID"] = $array[$i]['pk'];
        $data["District"] = $array[$i]['district'];
        $data["Action"] = "<button type='button' class='btn btn-primary mr-2' onclick=\"edit_loc('" . $array[$i]['pk'] . "')\" data-toggle=\"modal\" data-target=\"#myModalEdit\">Edit</button>
                           <button type='button' class='btn btn-danger mr-2' onclick=\"delete_loc('" . $array[$i]['pk'] . "')\" >Delete</button>";


        array_push($array_data, $data);
        $no++;
    }

    $output = array(
        "draw" => intval($_GET["draw"]),
        "recordsTotal" => intval($count['total']),
        "recordsFiltered" => intval($count['total']),
        "data" => $array_data
    );


    echo json_encode($output);
    mysqli_close($link);
}

function AddLoc()
{
    include "connection.php";

    $loc = $_POST['loc'];
    $district = $_POST['district'];

    $q = "INSERT INTO tbl_location (pk,district) VALUES ('$loc','$district')";
    $query = $link->query($q); 

    if ($query) {
        $result = array('success' => true, 'msg' => 'Success', 'msql'=>$q);
    } else {
        $result = array('success' => false, 'msg' => 'Error', 'msql'=>$q);
    }

    mysqli_close($link);
    echo json_encode($result);
}


function EditLoc()
{
    include "connection.php";

    $loc = $_POST['e_loc'];
    $loc_old = $_POST['e_loc_old'];
    $district = $_POST['e_district'];

    $q = "UPDATE `smsbc`.`tbl_location` SET `pk`='$loc', `district`='$district' WHERE `pk`='$loc_old';";
    $query = $link->query($q); 

    if ($query) {
        $result = array('success' => true, 'msg' => 'Success', 'msql'=>$q);
    } else {
        $result = array('success' => false, 'msg' => 'Error', 'msql'=>$q);
    }

    mysqli_close($link);
    echo json_encode($result);
}

function LocId()
{
    include "connection.php";

    $id = $_GET['id'];

    $q = "SELECT * FROM tbl_location WHERE pk='$id';";
    $query = $link->query($q); 

    $data = $query->fetch_assoc();

    if ($query) {
        $result = array('success' => true, 'msg' => 'Success', 'msql'=>$q, 'data'=>$data);
    } else {
        $result = array('success' => false, 'msg' => 'Error', 'msql'=>$q);
    }

    echo json_encode($result);
    mysqli_close($link);
}

function DeleteLoc()
{
    include "connection.php";

    $id = $_GET['id'];

    $q = "DELETE FROM tbl_location WHERE pk=$id";
    $query = $link->query($q); 

    if ($query) {
        $result = array('success' => true, 'msg' => 'Success', 'msql'=>$q);
    } else {
        $result = array('success' => false, 'msg' => 'Error', 'msql'=>$q);
    }

    echo json_encode($result);
    mysqli_close($link);
}

