<?php
date_default_timezone_set('asia/jayapura');

if (function_exists($_GET['f'])) {
    $_GET['f']();
}

function getDataLocation()
{
    include "connection.php";

    $search = '';

    if(isset($_GET['search'])){
        $search = $_GET['search'];
    }
    $teks = "select distinct district from tbl_location where district like '%$search%'";

    $query = $link->query($teks);

    if ($query->num_rows > 0) {
        $list = array();
        $key = 0;
        while ($row = $query->fetch_assoc()) {
            $list[$key]['id'] = $row['district'];
            $list[$key]['text'] = $row['district'];
            $key++;
        }
        echo json_encode($list);
    } else {
        echo "hasil kosong";
    }

    $link->close();
}
