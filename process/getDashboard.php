<?php
require '../predis/autoload.php';
date_default_timezone_set('asia/jayapura');

if (function_exists($_GET['f'])) {
  $_GET['f']();
}

function getDataRedis()
{
  try {
    $client = new Predis\Client(array(
      'host' => '127.0.0.1',
      'port' => 6379,
    ));
    if ($client) {

      $resp = $client->dbsize();

      // foreach ($data as $key => $value) {
      //   echo "{$key} => {$value}";
      //   // var_dump($key);
      // }
      $data = json_encode($resp);
      echo $data;
    } else {
      throw new Exception('Unable to connect');
    }
    $client->disconnect();
  } catch (Exception $e) {
    echo $e->getMessage();
  }
}

function getDataBroadcast()
{
  include "connection.php";

  $tgl = date('Y-m-d');

  $teks1 = "select count(id), status from tbl_smsbroadcast_student where DATE(schedule) = '$tgl' group by status;";
  // $teks2 = "select count(status) from  tbl_smsbroadcast_student where status = 'D' and  cast(created_date as Date) = cast(now() as Date);";
  // $teks3 = "select count(status) from  tbl_smsbroadcast_student where status = 'P' or status = 'D' and  cast(created_date as Date) = cast(now() as Date);";

  $query1 = $link->query($teks1);
  // $query2 = $link->query($teks2);
  // $query3 = $link->query($teks3);

  if ($query1) {
    $result = array('success' => true, 'data' => array(), 'msg' => 'Success', 'query'=>$teks1);
    while ($row = $query1->fetch_assoc()) {
      $data["status"] = $row['status'];
      $data["count"] = $row['count(id)'];
      array_push($result['data'], $data);
    }
    // while ($row = $query2->fetch_assoc()) {
    //   array_push($result['data'], $row);
    // }
    // while ($row = $query3->fetch_assoc()) {
    //   array_push($result['data'], $row);
    // }
  } else {
    $result = array('success' => false, 'msg' => 'Failed');
  }

  echo json_encode($result);
}

function getDataDU()
{
  include "connection.php";

  $tgl = date('Y-m-d');

  //$tgl = new DateTime($tgl);

  //$tgl->modify('+1 day');

  //$tgl=$tgl->format('Y-m-d');

  $teks1 = "SELECT 
              SUM(sms_dr) as dr, sum(sms_undelivery) as udr
            FROM
              tbl_smsbroadcast_student
            WHERE
              DATE(schedule) = '$tgl';";

  $query1 = $link->query($teks1);

  if ($query1) {
    $result = array('success' => true, 'data' => array(), 'msg' => 'Success', 'query'=>$teks1);
    $row = $query1->fetch_assoc();
    array_push($result['data'],$row);
    // while ($row = $query1->fetch_assoc()) {
    //   $data["status"] = $row['status'];
    //   $data["count"] = $row['count(id)'];
    //   array_push($result['data'], $data);
    // }
  } else {
    $result = array('success' => false, 'msg' => 'Failed');
  }

  echo json_encode($result);
}


function getDataSubmit()
{
  include "connection.php";

  $tgl = date('Y-m-d');

  $teks1 = "SELECT 
              SUM(sms_sm) AS total
            FROM
              tbl_smsbroadcast_student
            WHERE
              DATE(schedule) = '$tgl';";

  $query1 = $link->query($teks1);

  if ($query1) {
    $result = array('success' => true, 'data' => array(), 'msg' => 'Success', 'query'=>$teks1);
    while ($row = $query1->fetch_assoc()) {
      $data = $row['total'];
      array_push($result['data'], $data);
    }
  } else {
    $result = array('success' => false, 'msg' => 'Failed');
  }

  echo json_encode($result);
}

function getDataMIQ()
{
  $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_PORT           => "8080",
        CURLOPT_URL            => "http://".$_SERVER['HTTP_HOST']."/smsbc_student/countMessageInQueue",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING       => "",
        CURLOPT_MAXREDIRS      => 10,
        CURLOPT_TIMEOUT        => 30,
        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST  => "GET",
        CURLOPT_HTTPHEADER     => array(
            "cache-control: no-cache"
        ),
    ));

    $response = curl_exec($curl);
    $err      = curl_error($curl);

  echo json_encode($response);
}
