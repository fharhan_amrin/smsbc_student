<?php
// $servername = "150.242.111.235";
// $username = "root";
// $password = "r00t@dm1n05";
// $dbname = "db_smsbc";
include "connection.php";

date_default_timezone_set('asia/jayapura');
header('Access-Control-Allow-Origin:*');
header("Access-Control-Allow-Credentials: true");
header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
header('Access-Control-Max-Age: 1000');
header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');



$columns = array("No", "Sender", "MSISDN", "Submit Date", "Delivery Date", "Status"); // data array columns harus sama dengan data header table yang tadi di buat di view

$start = $_POST["start"]; //0
$length = $_POST["length"]; //10
$cari_data = $_POST["search"];
$cari_data = (string)$cari_data['value'];

//var_dump("SELECT * FROM tbl_cdr WHERE msisdn LIKE '%$cari_data%' LIMIT $start,$length"); die();


if (empty($cari_data)) {
    $dt = $link->query("SELECT * FROM tbl_cdr LIMIT $start,$length");
    $total = $link->query("SELECT COUNT(pk) as total FROM tbl_cdr");
} else {
    $dt = $link->query("SELECT * FROM tbl_cdr WHERE msisdn LIKE '%$cari_data%' LIMIT $start,$length");
    $total = $link->query("SELECT COUNT(pk) as total FROM tbl_cdr WHERE msisdn LIKE '%$cari_data%'");
}

$count = mysqli_fetch_array($total, MYSQLI_ASSOC);

$array = array();

while ($row = mysqli_fetch_array($dt, MYSQLI_ASSOC)) {
    array_push($array, $row);
}

$array_data = array();

//print_r($array);die();

$no = $start + 1;
for ($i = 0; $i < count($array); $i++) {
    $data["No"] = $no;
    $data["Sender"] = $array[$i]['sender'];
    $data["MSISDN"] = $array[$i]['msisdn'];
    $data["Submit Date"] = $array[$i]['submitdate'];
    $data["Delivery Date"] = $array[$i]['delivertdate'];
    $data["Status"] = '';

    if($array[$i]['finaldtatus']=='4'){
        $data["Status"] = 'Undelivered';
    }else if($array[$i]['finaldtatus']=='1'){
        $data["Status"] = 'Delivered';
    }

    array_push($array_data, $data);
    $no++;
}

function subString($text)
{
    if (strlen($text) > 40) {
        return substr($text, 0, 40) . ". . .";
    }else{
        return $text;
    }
    
}
// foreach ($dt->fetch_assoc() as $key) {

//     $data["No"]=$no;
//     $data["Destination Number"]=$key->destination_number;
//     $data["Origin Number"]=$key->origin_number;

//     array_push($array_data,$data);
//     $no++;

// }


//settingan terpenting dari datatable

$output = array(

    "draw" => intval($_POST["draw"]),
    "recordsTotal" => intval($count['total']),
    "recordsFiltered" => intval($count['total']),
    "data" => $array_data
);

echo json_encode($output);
//}
mysqli_close($link);
